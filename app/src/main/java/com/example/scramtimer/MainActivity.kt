package com.example.scramtimer

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.example.scramtimer.pagerLayotRecycleView.CustomItemTouchHelperCallback
import com.example.scramtimer.pagerLayotRecycleView.OnStartDragListener
import com.example.scramtimer.pagerLayotRecycleView.SlideAdapter
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnStartDragListener {
    //TODO tv_blocking_model text underline add
    private lateinit var rvAdapter: SlideAdapter
    private val requestCodeActivity = 42
    private var touchHelper: ItemTouchHelper? = null

    companion object{
        val TAG = MainActivity::class.java.name

        fun calculateNoOfColumns(
            context: Context,
            columnWidthDp: Float
        ): Int {
            val displayMetrics = context.resources.displayMetrics
            val screenWidthDp = displayMetrics.heightPixels / displayMetrics.density
            return (screenWidthDp / columnWidthDp + 0.5).toInt() - 1
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initRecyclerView()
        btn_add_item.setOnClickListener {
            startActivityForResult(
                Intent(
                    this,
                    CardInflater::class.java
                ),
                requestCodeActivity
            )
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK){
            rvAdapter.notifyItemInserted(rvAdapter.dataSize() + 1)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    // disable going back to the SplashActivity
    override fun onBackPressed() {
        moveTaskToBack(true)
    }

    override fun onPause() {
        super.onPause()
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        touchHelper?.startDrag(viewHolder);
    }

    private fun initRecyclerView() {
        rvAdapter = SlideAdapter()

        val spanCount = calculateNoOfColumns(this, 250f)
        val defaultLayoutManager = WrapContentLinearLayoutManager(this, spanCount)
        defaultLayoutManager.orientation = RecyclerView.HORIZONTAL

        rv_items.setHasFixedSize(true)
        rv_items.adapter = rvAdapter

        rv_items.layoutManager = defaultLayoutManager

        val callback = CustomItemTouchHelperCallback(rvAdapter)
        touchHelper = ItemTouchHelper(callback)
        touchHelper?.attachToRecyclerView(rv_items)
    }

    class WrapContentLinearLayoutManager(context: Context, spanCount: Int) : GridLayoutManager(context, spanCount) {
        override fun onLayoutChildren(recycler: RecyclerView.Recycler, state: RecyclerView.State) {
            try {
                super.onLayoutChildren(recycler, state)
            } catch (e: IndexOutOfBoundsException) {
                Log.e("TAG", "meet a IOOBE in RecyclerView")
            }
        }
    }
}
