package com.example.scramtimer

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.res.ResourcesCompat
import com.example.scramtimer.Data.CardData
import com.example.scramtimer.Data.CardField
import com.example.scramtimer.Data.TimerDataBase
import com.example.scramtimer.timer.Timer
import com.example.scramtimer.timer.TimerState
import io.realm.ObjectChangeSet
import io.realm.Realm
import io.realm.RealmObjectChangeListener
import io.realm.kotlin.addChangeListener
import kotlinx.android.synthetic.main.activity_show_card.*
import kotlinx.android.synthetic.main.pv_item.view.*
import java.text.SimpleDateFormat
import java.util.*

class ShowCard : AppCompatActivity() {
    private val realm = Realm.getDefaultInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_show_card)

        val listener = object : RealmObjectChangeListener<CardData> {
            override fun onChange(data: CardData, changeSet: ObjectChangeSet?) {
                if (changeSet == null || changeSet.isDeleted) {
                    return
                }

                for (fieldName in changeSet.changedFields) {
                    Log.i("Tag", "Field $fieldName was changed.")
                    if (fieldName == CardField.CARD_STATE.nameField){
                        updateButton(data.supportEnumState)
                    }
                    if(fieldName == CardField.CARD_TIME_LEFT.nameField){
                        tv_time.text = Timer.parseMilliToTime(data.timeLeft)
                    }
                }
            }
        }

        val cardName = intent.extras!!.getString(CardField.CARD_NAME.nameField)!!
        val data = TimerDataBase.getCardByName(cardName)
        data.addChangeListener(listener)

        this.tv_show_blocking_model.text = data.nameCard
        this.tv_show_description.text = data.descriptionCard
        this.tv_time.text = Timer.parseMilliToTime(data.timeLeft)
        this.tv_date.text = getDate(data)


        if(data.supportEnumState == TimerState.Running){
            updateButton(data.supportEnumState)
        }else{
            this.tv_time.text = Timer.parseMilliToTime(data.timeLeft)
            updateButton(data.supportEnumState)
        }

        iv_btn_delete.setOnClickListener {

            val builder = AlertDialog.Builder(this, R.style.CustomDialogTheme )
            builder.setTitle("Delete")
            builder.setMessage("Do you want to delete this card?")

            builder.setPositiveButton(android.R.string.yes) { _, _ ->
                Timer.onTimerDelete()
                TimerDataBase.deleteItem(data)
                finish()
            }

            builder.setNegativeButton(android.R.string.no) { _, _ -> }

            builder.show()
        }

        this.iv_btn_start_pause.setOnClickListener {
            if(data.supportEnumState == TimerState.Running){
                this.stopService(Intent(this, Timer::class.java))
            }else{
                this.startService(Intent(this, Timer::class.java).apply {
                    putExtra(CardField.CARD_NAME.name, data.nameCard)
                })
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    private fun updateButton(timerState: TimerState){
        when(timerState){
            TimerState.Stopped, TimerState.Paused -> {
                this.iv_btn_start_pause.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_play_arrow_black_24dp,
                        theme
                    ))
                this.iv_btn_start_pause.background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_btn_timer_pause,
                    theme
                )
            }
            TimerState.Running -> {
                this.iv_btn_start_pause.setImageDrawable(
                    ResourcesCompat.getDrawable(
                        resources,
                        R.drawable.ic_pause_black_24dp,
                        theme
                    ))
                this.iv_btn_start_pause.background = ResourcesCompat.getDrawable(
                    resources,
                    R.drawable.ic_btn_timer_running,
                    theme
                )
            }
        }
    }

    private fun getDate(data: CardData): String{
        val patternDate = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        val dateToString = patternDate.format(data.dateOfAdd)
        return "Добавлена $dateToString"
    }

}
