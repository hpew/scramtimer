package com.example.scramtimer

import android.app.Activity
import android.app.TimePickerDialog
import android.os.Bundle
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.example.scramtimer.Data.CardData
import com.example.scramtimer.Data.TimerDataBase
import com.example.scramtimer.timer.TimeSelect
import com.example.scramtimer.timer.TimerState
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_card_inflater.*
import java.text.SimpleDateFormat
import java.util.*

const val toMillisecond = 60 * 60 * 1000

class CardInflater : AppCompatActivity() {
    private var nameCard: String? = ""
    private var descriptionCard: String? = ""
    private var timeSelect: Long = 7200000L
    private val realm = Realm.getDefaultInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_card_inflater)

        tv_select_custom_time.text = com.example.scramtimer.timer.Timer.parseMilliToTime(
            TimeSelect.values()[sb_card_select_time.progress].time * toMillisecond.toLong()
        )

        sb_card_select_time.setOnSeekBarChangeListener( object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                tv_select_custom_time.text = when(progress){
                        in 0..3 -> {com.example.scramtimer.timer.Timer.parseMilliToTime(
                            TimeSelect.values()[progress].time * toMillisecond.toLong()
                        )}
                        else    -> {com.example.scramtimer.timer.Timer.parseMilliToTime(
                            (1 * toMillisecond).toLong()
                        )}
                }
                timeSelect = when(sb_card_select_time.progress){
                    in 0..3 -> (TimeSelect.values()[sb_card_select_time.progress].time * toMillisecond).toLong()
                    else    -> (1 * toMillisecond).toLong()
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {}

            override fun onStopTrackingTouch(seekBar: SeekBar?) {}

        })

        btn_add_card.setOnClickListener {
            nameCard = et_card_name.text.toString()
            descriptionCard = et_card_description.text.toString()

            addCard()
        }

        btn_select_custom_time.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { _, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                tv_select_custom_time.text = SimpleDateFormat("HH:mm", Locale.getDefault()).format(cal.time)
                timeSelect = (hour * toMillisecond + minute * 60 * 1000).toLong()
            }
            TimePickerDialog(
                this,
                R.style.CustomDialogTheme,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                true
            ).show()

        }
    }

    private fun addCard() {
        if (!cardDataIsValidate()){
            return
        }

        val dateOfAddToCalendar = Calendar.getInstance()
        dateOfAddToCalendar.timeInMillis = Calendar.getInstance().timeInMillis
        val newCard = CardData(nameCard!!, descriptionCard!!, dateOfAddToCalendar.time, timeSelect)
        newCard.supportEnumState = TimerState.Stopped
        newCard.timeLeft = timeSelect
        TimerDataBase.addOrUpdateItem(newCard)

        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun cardDataIsValidate(): Boolean {
        var validate = true
        if(nameCard.isNullOrEmpty()){
            validate = false
            et_card_name.error = "Name must not be empty."
        }
        if(descriptionCard.isNullOrEmpty()){
            validate = false
            et_card_description.error = "Description must not be empty."
        }
        //TODO if add network service, you should replace primary key
        val result = realm.where<CardData>()
            .equalTo("nameCard", nameCard).findFirst()
        if(result != null){
            validate = false
            et_card_name.error = "Card with this name created."
        }
        return validate
    }
}
