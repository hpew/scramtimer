package com.example.scramtimer

import android.content.Intent
import android.graphics.drawable.Animatable
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import io.realm.Realm
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {
    private val IT_IS_A_MAGIC_DURATION = 750L
    private val IT_IS_A_MAGIC = 500L
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        // Initialize Realm
        Realm.init(this)

        //TODO support pre version animation
        if(Build.VERSION.SDK_INT >= 21) {
            iv_timer.setImageDrawable(resources.getDrawable(R.drawable.timer_cut_anim, this.theme))
            val drawableTimer = iv_timer.drawable
            val animTimer = drawableTimer as Animatable
            Handler().postDelayed(
                {
                    animTimer.start()
                },
                IT_IS_A_MAGIC)
            iv_timer.setOnClickListener {
                animTimer.start()
            }
        }
        Handler().postDelayed(
            {
                startActivity(Intent(this, MainActivity::class.java))
            },
            IT_IS_A_MAGIC_DURATION + IT_IS_A_MAGIC)
    }
}