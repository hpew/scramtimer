package com.example.scramtimer.timer

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.media.MediaPlayer
import android.os.Build
import android.os.CountDownTimer
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MIN
import com.example.scramtimer.Data.CardData
import com.example.scramtimer.Data.CardField
import com.example.scramtimer.Data.TimerDataBase
import com.example.scramtimer.MainActivity
import com.example.scramtimer.R
import java.util.*
import java.util.concurrent.TimeUnit


class Timer : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("ZAEBINF", "START_COMMAND")
        startForegroundService()
        val nameCard = intent?.extras?.getString(CardField.CARD_NAME.name, "nun_name")
        if(nameCard == "nun_name") return super.onStartCommand(intent, flags, startId)
        val cardData = TimerDataBase.getCardByName(nameCard!!)
        initTimer(cardData)

        return START_STICKY
    }

    override fun onDestroy() {
        pauseTimer()
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    private fun getBuilderNotification(): NotificationCompat.Builder {
        val channelId =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                createNotificationChannel("timer_service", "BackgroundService")
            } else {
                ""
            }

        return NotificationCompat.Builder(this, channelId )
    }

    private fun getPendingActivity(): PendingIntent {
        val resultIntent = Intent(this, MainActivity::class.java)
        return PendingIntent.getActivity(
            this, 0, resultIntent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
    }

    private fun startForegroundService() {
        val notificationBuilder = getBuilderNotification()

        val resultPendingIntent = getPendingActivity()

        val notification = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_time_cut)
                .setPriority(PRIORITY_MIN)
                .setCategory(Notification.CATEGORY_SERVICE)
                .setContentTitle(this.resources.getString(R.string.timer))
                .setContentIntent(resultPendingIntent)
                .setContentText("00:00")
                .build()
        } else{
            notificationBuilder.setOngoing(true)
                .setSmallIcon(R.drawable.ic_time_cut)
                .setPriority(PRIORITY_MIN)
                .setContentTitle(this.resources.getString(R.string.timer))
                .setContentIntent(resultPendingIntent)
                .setContentText("00:00")
                .build()
        }

        startForeground(ID_SERVICE, notification)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String{
        val chan = NotificationChannel(channelId,
            channelName, NotificationManager.IMPORTANCE_NONE)
        chan.lightColor = Color.BLUE
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }

    private fun initTimer(data: CardData){
        Log.d("ZAEBINF", "INIT_TIMER")
        when{
            workCardData == null -> {
                workCardData = data
            }
            workCardData!!.nameCard != data.nameCard -> {
                pauseTimer()
                workCardData = data
            }
        }
        if(timer == null){
            startTimer()
        }
    }

    private fun startTimer(){
        TimerDataBase.listener {
            workCardData!!.supportEnumState = TimerState.Running
        }

        if(timer != null){
            timer?.cancel()
            timer = null
        }

        val notifyBuilder = getBuilderNotification()
        val timerText = this.resources.getString(R.string.timer)
        val resultPendingIntent = getPendingActivity()

        timer = object : CountDownTimer(workCardData!!.timeLeft, 1000) {
            override fun onFinish() = onTimerFinished()

            override fun onTick(millisUntilFinished: Long) {
                Log.d("ZAEBINF", "Time left: $millisUntilFinished")

                val notification = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    notifyBuilder.setOngoing(true)
                        .setSmallIcon(R.drawable.ic_time_cut)
                        .setPriority(PRIORITY_MIN)
                        .setCategory(Notification.CATEGORY_SERVICE)
                        .setContentTitle(timerText)
                        .setContentIntent(resultPendingIntent)
                        .setContentText(parseMilliToTime(millisUntilFinished))
                        .build()
                } else{
                    notifyBuilder.setOngoing(true)
                        .setSmallIcon(R.drawable.ic_time_cut)
                        .setPriority(PRIORITY_MIN)
                        .setContentTitle(timerText)
                        .setContentIntent(resultPendingIntent)
                        .setContentText(parseMilliToTime(millisUntilFinished))
                        .build()
                }

                startForeground(ID_SERVICE, notification)

                TimerDataBase.listener {
                    workCardData!!.timeLeft = millisUntilFinished
                    workCardData!!.supportEnumState = TimerState.Running
                }
            }
        }.start()
    }

    private fun pauseTimer(){
        Log.d("ZAEBINF", "Timer PAUSE: ${workCardData!!.timeLeft}")
        timer?.cancel()
        timer = null
        TimerDataBase.listener {
            workCardData!!.timeLastPause = Calendar.getInstance().time
            workCardData!!.supportEnumState = TimerState.Paused
        }
        workCardData = null
    }

    private fun onTimerFinished(){
        Log.d("ZAEBINF", "Timer FINISHED: ${workCardData!!.timeLeft}")
        val mediaPlayer = MediaPlayer.create(this, R.raw.finish_song)
        mediaPlayer.setOnPreparedListener {
            mediaPlayer.start()
        }
        TimerDataBase.listener {
            workCardData!!.timeStop = Calendar.getInstance().time
            workCardData!!.supportEnumState = TimerState.Stopped
        }
    }

    companion object{
        private const val ID_SERVICE = 42424242
        private val TAG = Timer::class.java.name
        private var workCardData: CardData? = null
        private var timer: CountDownTimer? = null

        fun parseMilliToTime(millis: Long): String {
            if((TimeUnit.MILLISECONDS.toSeconds(millis)) == 0L){
                return "Dead!"
            }

            var hoursStr = "${TimeUnit.MILLISECONDS.toHours(millis)}"
            var mintsStr = "${TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))}"
            return if(hoursStr == "0" && mintsStr == "0"){
                "${TimeUnit.MILLISECONDS.toSeconds(millis)}"
            }else {
                if (hoursStr.length == 1) hoursStr = "0$hoursStr"
                if (mintsStr.length == 1) mintsStr = "0$mintsStr"
                "$hoursStr:$mintsStr"
            }
        }

        fun onTimerDelete(){
            Log.d("ZAEBINF", "Timer DELETE")
            if (workCardData == null){
                return
            }
            if(timer != null){
                timer?.cancel()
                timer = null
            }
            TimerDataBase.listener {
                workCardData!!.supportEnumState = TimerState.Stopped
            }
            workCardData = null
        }
    }
}