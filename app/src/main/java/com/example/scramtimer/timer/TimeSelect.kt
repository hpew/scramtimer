package com.example.scramtimer.timer

enum class TimeSelect(val time: Int) {
    TWO(2),
    FOUR(4),
    EIGHT(8),
    SIXTEEN(16)
}