package com.example.scramtimer.timer

enum class TimerState{
    Stopped, Paused, Running
}