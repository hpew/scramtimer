package com.example.scramtimer.Data

enum class CardField(val nameField: String) {
    CARD_NAME("nameCard"),
    CARD_DESCRIPTION("descriptionCard"),
    CARD_DATE_OF_ADD("dateOfAdd"),
    CARD_TIME_SELECT("timeSelect"),
    CARD_TIME_START("timeStart"),
    CARD_TIME_STOP("timeStop"),
    CARD_TIME_LAST_PAUSE("timeLastPause"),
    CARD_TIME_LEFT("timeLeft"),
    CARD_STATE("state")
}