package com.example.scramtimer.Data

import io.realm.RealmList
import io.realm.RealmObject
import java.io.Serializable

open class CardList (
    var cardRealmList: RealmList<CardData> = RealmList()
): RealmObject(), Serializable