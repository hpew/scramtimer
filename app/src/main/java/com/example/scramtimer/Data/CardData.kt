package com.example.scramtimer.Data

import com.example.scramtimer.timer.TimerState
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable
import java.util.*

open class CardData(
    //TODO If add network service, you should replace primary key
    //TODO Also add time delete and refactor logic delete Card to TimerDataBase
    @PrimaryKey var nameCard: String = "",
    var descriptionCard: String = "",
    var dateOfAdd: Date = Date(),
    var timeSelect: Long = 0L
): RealmObject(), Serializable{

    var timeStart: Date = Date()
    var timeStop: Date = Date(-1L)

    var timeLastPause: Date = Date(-1L)

    var timeLeft: Long = -1L

    // Public field exposing setting/getting the enum
    private var state: String = TimerState.Stopped.name

    var supportEnumState: TimerState
        get() = TimerState.values().first { it.name == state }
        set(value) {
            state = value.name
        }
//    // Public field exposing setting/getting the enum
//    var supportEnumTimeSelect: TimeSelect
//        get() = TimeSelect.values().first { it.name == state }
//        set(value) {
//            state = value.name
//        }
}