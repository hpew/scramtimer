package com.example.scramtimer.Data

import android.util.Log
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmResults
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import java.util.*

class TimerDataBase{
    companion object{
        private val TAG = TimerDataBase::class.java.name
        private val realm = Realm.getDefaultInstance()
        private lateinit var cardList: CardList

        fun getRealmResults(): RealmResults<CardData> {
            return realm.where<CardData>().findAll()!!
        }

        fun getListRealmResults(): RealmList<CardData> {
            val realmResults = realm.where<CardData>().findAll()!!

            realm.beginTransaction()
            cardList = realm.createObject<CardList>()
            cardList.cardRealmList.addAll(realmResults)
            realm.commitTransaction()

            return cardList.cardRealmList
        }

        fun getCardByName(nameCard: String): CardData {
            return realm.where<CardData>()
                .equalTo(
                    CardField.CARD_NAME.nameField,
                    nameCard
                ).findFirst()!!
        }

        fun deleteItem(card: CardData){
            realm.executeTransaction{
                card.deleteFromRealm()
            }
        }
        fun addOrUpdateItem(card: CardData){
            Log.d("HERERE", "add: $card")
            realm.executeTransaction{
                cardList.cardRealmList.add((realm.copyToRealmOrUpdate(card)))
            }
        }

        fun listener(action: () -> Unit) {
            realm.executeTransaction{
                action.invoke()
            }
        }

    }
}
