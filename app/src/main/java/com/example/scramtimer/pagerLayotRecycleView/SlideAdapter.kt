package com.example.scramtimer.pagerLayotRecycleView

import android.app.AlertDialog
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.scramtimer.CardInflater
import com.example.scramtimer.Data.CardData
import com.example.scramtimer.Data.CardField
import com.example.scramtimer.Data.TimerDataBase
import com.example.scramtimer.MainActivity
import com.example.scramtimer.R
import com.example.scramtimer.ShowCard
import com.example.scramtimer.timer.Timer
import com.example.scramtimer.timer.TimerState
import io.realm.ObjectChangeSet
import io.realm.RealmObjectChangeListener
import kotlinx.android.synthetic.main.pv_item.view.*
import java.text.SimpleDateFormat
import java.util.*

//private val dragStartListener: DragStartHelper.OnDragStartListener
class SlideAdapter(): RecyclerView.Adapter<SlideAdapter.SlideHolder>(), ItemTouchHelperAdapter {
    private var data = TimerDataBase.getListRealmResults()

    companion object {
        private val TAG = SlideAdapter::class.simpleName
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SlideHolder {
        val resType = if(viewType == 0) R.layout.pv_item else R.layout.pv_item_empty
        val inflateView = LayoutInflater.from(parent.context).inflate(resType, parent, false)

        return SlideHolder(inflateView)
    }

    override fun getItemViewType(position: Int): Int = if(position < data.size) 0 else 1

    override fun getItemCount(): Int = when{
        data.size == 0          -> {4}
        (data.size % 4) != 0    -> {data.size + (4 - (data.size % 4))}
        else                    -> {data.size}
    }

    override fun onBindViewHolder(holder: SlideHolder, position: Int) {

        if(position < data.size){
            if(data[position]!!.isValid) {
                holder.initHolder(position, data[position]!!,  ::notifyDataSetChanged) {deleteItem(holder.adapterPosition)}
            }
        }else{
            holder.itemView.setOnClickListener {
                (it.context as MainActivity).startActivityForResult(Intent(it.context, CardInflater::class.java), 42)
            }
        }

    }

    override fun onItemDismiss(position: Int) {
        if(position < data.size){
            TimerDataBase.deleteItem(data[position]!!)
            deleteItem(position)
        }
    }

    override fun onItemMove(fromPosition: Int, toPosition: Int): Boolean {
        return if(fromPosition < data.size && toPosition < dataSize()) {
            if (fromPosition < toPosition) {
                for (i in fromPosition until toPosition) {
                    TimerDataBase.listener {
                        data.move(i, i + 1)
                    }
                }
            } else {
                for (i in fromPosition downTo toPosition + 1) {
                    TimerDataBase.listener {
                        data.move(i, i - 1)
                    }
                }
            }
            notifyItemMoved(fromPosition, toPosition)
            true
        }else{false}
    }

    //TODO refactor code(scroll to slide)
    class SlideHolder(
        itemView: View
    ) : RecyclerView.ViewHolder(itemView) {

        private var context = itemView.context!!

        fun initHolder(position: Int, data: CardData, notify: () -> Unit, deleteItem: () -> Unit) {
            val listener = object : RealmObjectChangeListener<CardData> {
                override fun onChange(data: CardData, changeSet: ObjectChangeSet?) {
                    if (changeSet == null || changeSet.isDeleted) {
                        deleteItem()
                        return
                    }

                    for (fieldName in changeSet.changedFields) {
                        Log.i("Tag", "Field $fieldName was changed.")
                        if (fieldName == CardField.CARD_STATE.nameField){
                            updateButton(data.supportEnumState)
                        }
                        if(fieldName == CardField.CARD_TIME_STOP.nameField){
                            context.stopService(Intent(context, Timer::class.java))
                        }
                        if(fieldName == CardField.CARD_TIME_LEFT.nameField){
                            itemView.tv_time.text = Timer.parseMilliToTime(data.timeLeft)
                        }
                    }
                }
            }

            data.addChangeListener(listener)
            itemView.tv_blocking_model.text = data.nameCard
            itemView.tv_time.text = Timer.parseMilliToTime(data.timeLeft)
            itemView.tv_date.text = getDate(data)

            updateButton(data.supportEnumState)

            itemView.setOnClickListener{
                context.startActivity(Intent(context,
                    ShowCard::class.java).apply {
                    putExtra(CardField.CARD_NAME.nameField, data.nameCard)
                })
            }

            itemView.iv_btn_delete.setOnClickListener {
                if(data.isValid){
                    val builder = AlertDialog.Builder(itemView.context, R.style.CustomDialogTheme )
                    builder.setTitle("Delete")
                    builder.setMessage("Do you want to delete this card?")

                    builder.setPositiveButton(android.R.string.yes) { _, _ ->
                        Timer.onTimerDelete()
                        TimerDataBase.deleteItem(data)
                    }

                    builder.setNegativeButton(android.R.string.no) { _, _ -> }

                    builder.show()
                }else{
                    notify()
                    val builder = AlertDialog.Builder(itemView.context, R.style.CustomDialogTheme )
                    builder.setTitle("Delete")
                    builder.setMessage("Do you want to delete this card?")

                    builder.setPositiveButton(android.R.string.yes) { _, _ ->
                        Timer.onTimerDelete()
                        TimerDataBase.deleteItem(data)
                        deleteItem()
                    }

                    builder.setNegativeButton(android.R.string.no) { _, _ -> }

                    builder.show()
                }
            }

            itemView.iv_btn_start_pause.setOnClickListener{
                if(data.supportEnumState == TimerState.Running){
                    context.stopService(Intent(context, Timer::class.java))
                }else{
                    Log.d("ZAEBINF", "btn CLICK")
                    context.startService(Intent(context, Timer::class.java).apply {
                        putExtra(CardField.CARD_NAME.name, data.nameCard)
                    })
                }
            }
        }

        private fun updateButton(timerState: TimerState){
            when(timerState){
                TimerState.Stopped, TimerState.Paused -> {
                    itemView.iv_btn_start_pause.setImageDrawable(
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_play_arrow_black_24dp,
                            context.theme
                        ))
                    itemView.iv_btn_start_pause.background = ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_btn_timer_pause,
                        context.theme
                    )
                }
                TimerState.Running -> {
                    itemView.iv_btn_start_pause.setImageDrawable(
                        ResourcesCompat.getDrawable(
                            context.resources,
                            R.drawable.ic_pause_black_24dp,
                            context.theme
                        ))
                    itemView.iv_btn_start_pause.background = ResourcesCompat.getDrawable(
                        context.resources,
                        R.drawable.ic_btn_timer_running,
                        context.theme
                    )
                }
            }
        }

        private fun getDate(data: CardData): String{
            val patternDate = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
            val dateToString = patternDate.format(data.dateOfAdd)
            return "Добавлена $dateToString"
        }
    }

    fun addItem(){
//        TimerDataBase.addOrUpdateItem(newCard)
        notifyDataSetChanged()
    }

    fun addItems(newCards: ArrayList<CardData>){
        for(newCard in newCards){
            TimerDataBase.addOrUpdateItem(newCard)
        }
        notifyDataSetChanged()
    }

    private fun deleteItem(position: Int){
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, itemCount)
    }

    fun dataSize(): Int = data.size
}