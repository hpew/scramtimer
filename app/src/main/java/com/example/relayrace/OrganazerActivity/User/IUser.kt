package com.example.relayrace.OrganazerActivity.User

interface IUser {
    fun validate(): Boolean
    fun userIsFound(login: String, password: String): Boolean
}