package com.example.relayrace.OrganazerActivity.Etap

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SeekBar
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.example.relayrace.OrganazerActivity.QUANTITY_OF_ETAP
import com.example.relayrace.R

class ListViewEtapAdapter(private val context: Context): BaseAdapter() {
    private var dataSource: ArrayList<EtapResources> = ArrayList()
    private var infoEtap = ""
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        if (position == 0){
            val rowView = inflater.inflate(R.layout.lv_header, parent, false)
            val tvInfo = rowView.findViewById<AppCompatTextView>(R.id.tv_header_info)
            rowView.visibility = View.VISIBLE
            tvInfo.visibility = View.VISIBLE
            tvInfo.text = infoEtap
            return rowView
        }
        val rowView = inflater.inflate(R.layout.listview_item_participants, parent, false)
        val dataItem = getItem(position) as EtapResources
        if(dataItem.disable_btn != 0){
            rowView.visibility = View.INVISIBLE
            return rowView
        }else{
            rowView.visibility = View.VISIBLE
        }
        val tvGroupName = rowView.findViewById<AppCompatTextView>(R.id.tv_group_name)
        val tvEtap = rowView.findViewById<AppCompatTextView>(R.id.tv_etap)
        val sbEtap = rowView.findViewById<androidx.appcompat.widget.AppCompatSeekBar>(R.id.sb_etap)

        tvGroupName.text = (dataItem.name)
        tvEtap.text = sbEtap.progress.toString()

        sbEtap.max = QUANTITY_OF_ETAP
        sbEtap.isClickable = false
        sbEtap.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(sb: SeekBar?, position: Int, flag: Boolean) {
                tvEtap.text = position.toString()
                sb!!.isClickable = false
            }

            override fun onStartTrackingTouch(sb: SeekBar?) {sb!!.isClickable = false}

            override fun onStopTrackingTouch(sb: SeekBar?) {sb!!.isClickable = false}
        })

        val colorView = when(dataItem.color){
            "red"->     ContextCompat.getColor(context, R.color.red)
            "orange"->  ContextCompat.getColor(context, R.color.orange)
            "yellow"->  ContextCompat.getColor(context, R.color.yellow)
            "green"->   ContextCompat.getColor(context, R.color.green)
            "blue"->    ContextCompat.getColor(context, R.color.blue)
            else->      ContextCompat.getColor(context, android.R.color.background_light)
        }

        rowView.setBackgroundColor(colorView)

        return rowView
    }

    override fun isEnabled(position: Int): Boolean = if (position != 0) {dataSource[position - 1].disable_btn == 0} else {true}

    override fun getItem(position: Int): Any = if (position > 0){dataSource[position - 1]} else {0}

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = dataSource.size

    fun dataUpdate(newRes: DataEtapRequest){
        if(newRes.btn != null){
            this.dataSource = newRes.btn!!
        }else{
            this.dataSource = ArrayList()
        }
        if(newRes.info != null && newRes.info != newRes.info!!){
            this.infoEtap = newRes.info!!
        }
        notifyDataSetChanged()
    }

    fun clearElements(){
        dataSource.clear()
        notifyDataSetChanged()
    }
}