package com.example.relayrace.OrganazerActivity.Start

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.example.relayrace.R

class ListViewOrganizeAdapter(private val context: Context): BaseAdapter() {
    private var dataSource: ArrayList<StartResources> = ArrayList()
    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val rowView = inflater.inflate(R.layout.listview_item_races, parent, false)
        val tvCommandGroupName = rowView.findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tv_command_group_name)
        val tvEtapId = rowView.findViewById<androidx.appcompat.widget.AppCompatTextView>(R.id.tv_etap_id)

        val dataItem = getItem(position) as StartResources

        tvCommandGroupName.text = ("${tvCommandGroupName.text} ${dataItem.command_group_name}").toString()
        tvEtapId.text = ("${tvEtapId.text} ${dataItem.etap_id}").toString()

        return rowView
    }

    override fun getItem(position: Int): Any = dataSource[position]

    override fun getItemId(position: Int): Long = position.toLong()

    override fun getCount(): Int = dataSource.size

    fun dataUpdate(newRes: ArrayList<StartResources>){
        this.dataSource = newRes
        notifyDataSetChanged()
    }

    fun clearElements(){
        dataSource.clear()
        notifyDataSetChanged()
    }
}