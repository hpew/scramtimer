package com.example.relayrace.OrganazerActivity.User

data class DataUser(
    var stepNum: Int = -1,
    var fullName: String = "",
    var login: String = "",
    var password: String = ""
)

data class RequestUser(
    var auth: String?,
    var etap: Int?
)