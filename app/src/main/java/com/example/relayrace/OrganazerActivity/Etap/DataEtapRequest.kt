package com.example.relayrace.OrganazerActivity.Etap

data class DataEtapRequest(var btn: ArrayList<EtapResources>? = ArrayList(), var info: String?)
data class EtapResources(var name: String?, var c_id: Int?, var color: String?, var disable_btn: Int?)