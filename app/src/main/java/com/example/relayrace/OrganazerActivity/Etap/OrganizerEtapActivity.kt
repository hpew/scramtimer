package com.example.relayrace.OrganazerActivity.Etap

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.relayrace.OrganazerActivity.DURATION_UPDATE_SEK
import com.example.relayrace.OrganazerActivity.ETAP
import com.example.relayrace.OrganazerActivity.LOGIN
import com.example.relayrace.OrganazerActivity.PASSWORD
import com.example.relayrace.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_organizer_etap.*
import kotlinx.android.synthetic.main.lv_header.*
import okhttp3.*
import java.io.IOException

const val C_ID = "c_id"

fun View.action(flag: Boolean){
    if(flag){
        this.isEnabled = true
        this.isClickable = true
        this.isLongClickable = true
        this.visibility = View.VISIBLE
    }else{
        this.isEnabled = false
        this.isClickable = false
        this.isLongClickable = false
        this.visibility = View.INVISIBLE
    }
}

//TODO refactor
class OrganizerEtapActivity : AppCompatActivity() {
    val TAG_ETAP = "ETAP"
    val etapUrl = "https://relayrace.apisrv.ru/api/mobile/step.php"

    private var mHandler: Handler? = Handler()
    private lateinit var listViewEtap: ListView
    private lateinit var lveAdapter: ListViewEtapAdapter

    private var login = ""
    private var password = ""
    private var etap = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizer_etap)
        progress_bar_etap_organizer.visibility = View.VISIBLE

        login = intent.getStringExtra(LOGIN)!!
        password = intent.getStringExtra(PASSWORD)!!
        etap = intent.getIntExtra(ETAP, 0)
        listViewEtap = findViewById(R.id.lv_participants)
        lveAdapter = ListViewEtapAdapter(this)
        listViewEtap.adapter = lveAdapter
    }

    override fun onResume() {
        getHandler()

        permanentRequestEtapInfo.run()

        listViewEtap.setOnItemClickListener { adapterView, view, i, _ ->
            if(i == 0){
                return@setOnItemClickListener
            }
            view.action(false)
            progress_bar_etap_organizer.visibility = View.VISIBLE
            val commandId = (adapterView.getItemAtPosition(i) as EtapResources).c_id!!

            requestEtapInfo(commandId)
        }

        super.onResume()
    }

    override fun onPause() {
        getHandler().removeCallbacks(permanentRequestEtapInfo)
        super.onPause()
    }

    override fun onStop() {
        getHandler().removeCallbacks(permanentRequestEtapInfo)
        super.onStop()
    }

    override fun onDestroy() {
        getHandler().removeCallbacks(permanentRequestEtapInfo)
        super.onDestroy()
    }

    private fun createHeader(text: String?): View? {
        val v: View = layoutInflater.inflate(R.layout.lv_header, null)
        (v.findViewById<View>(R.id.tv_header_info) as TextView).text = text
        return v
    }

    private fun getHandler(): Handler{
        if(mHandler == null){
            mHandler = Handler()
        }
        return mHandler as Handler
    }

    private val permanentRequestEtapInfo: Runnable = object : Runnable{
        override fun run() {
            requestEtapInfo()
            getHandler().postDelayed(this, 1000 * DURATION_UPDATE_SEK)
        }
    }

    private fun requestEtapInfo(cId: Int = 0){
        val client = OkHttpClient()
        val request = Request.Builder()
            .get()
            .url("$etapUrl?$LOGIN=$login&$PASSWORD=$password&$C_ID=$cId")
            .build()

        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG_ETAP, e.message.toString())
                runOnUiThread {
                    Toast.makeText(baseContext, "Get data failed", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    val data = Gson().fromJson<DataEtapRequest>(response.body?.string(), DataEtapRequest::class.java)

                    runOnUiThread {
                        if (data is DataEtapRequest){
                            lveAdapter.dataUpdate(data)
                        }else{
                            lveAdapter.dataUpdate(DataEtapRequest(ArrayList(), "null"))
                        }
                        progress_bar_etap_organizer.visibility = View.INVISIBLE
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                    Log.e(TAG_ETAP, e.message.toString())
                    runOnUiThread{
                        Toast.makeText(baseContext, "Fail data parse", Toast.LENGTH_SHORT).show()
                    }
                }

            }

        })
    }
}
