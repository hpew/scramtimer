package com.example.relayrace.OrganazerActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.relayrace.OrganazerActivity.Etap.OrganizerEtapActivity
import com.example.relayrace.OrganazerActivity.Start.OrganizerStartActivity
import com.example.relayrace.OrganazerActivity.User.DataUser
import com.example.relayrace.OrganazerActivity.User.IUser
import com.example.relayrace.OrganazerActivity.User.RequestUser
import com.example.relayrace.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_organizer_login.*
import okhttp3.*
import java.io.IOException

const val ETAP = "etap"
const val LOGIN = "login"
const val PASSWORD = "password"
const val DURATION_UPDATE_SEK = 5L
const val QUANTITY_OF_ETAP = 13
const val checkUserUrl = "https://relayrace.apisrv.ru/api/mobile/login.php"
class OrganizerLoginActivity : AppCompatActivity(),
    IUser {

    private val TAG_OK = "ok"
    private val TAG_LOGIN = "login"
    private val mapDataUser = mapOf(
        0 to DataUser(7,"Вячеслав Сергеевич Рудковский","89064199500","0095"),
        1 to DataUser(1,"Мамедов Эмиль Валерьевич","89996990284","4802"),
        2 to DataUser(2,"Залуцкий Владимир Андреевич","89885818816","6188"),
        3 to DataUser(3,"Машков Александр Васильевич","89185107363","7363"),
        4 to DataUser(4,"Семикова Валерия Игоревна","89289088721","8721"),
        5 to DataUser(5,"Рыбалова Дарья Андреевна","89526089211","9211"),
        6 to DataUser(6,"Акользина Алина Алексеевна","89282894787","4787"),
        7 to DataUser(7,"Ибрагимов Владимир Дмитриевич","89094090317","0317"),
        8 to DataUser(8,"Подерягина Кристина Сергеевна","89185076620","6620"),
        9 to DataUser(9,"Леонов Игорь Сергеевич","89381024225","4225"),
        10 to DataUser(10,"Амельченко Александр Михайлович","89515191917","1917"),
        11 to DataUser(11,"Бариленко Станислав Николаевич","89185808687","8687")
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizer_login)

        btn_sign_in.setOnClickListener {
            login()
        }
    }

    private fun login() {
        tv_warning.text = ""
        val login = input_login.text.toString()
        val password = input_password.text.toString()
        val client = OkHttpClient()

        Log.d(TAG_LOGIN, "Login")

        if (!validate()) {
            return
        }

        val request = Request.Builder()
            .url("$checkUserUrl?$LOGIN=$login&$PASSWORD=$password")
            .get()
            .build()

        btn_sign_in.isEnabled = false
        progress_bar_login.visibility = View.VISIBLE

        client.newCall(request).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG_LOGIN,"request failed: ${e.message.toString()}")
                onLoginFailed()
            }

            override fun onResponse(call: Call, response: Response) {
                val result = Gson().fromJson<RequestUser>(response.body?.string(), RequestUser::class.java)
                Log.d(TAG_LOGIN, result.toString())
                runOnUiThread {
                    if (result.auth == TAG_OK) {
                        onLoginSuccess(result.etap!!, login, password)
                    } else {
                        tv_warning.text = getString(R.string.sign_in_fail)
                        onLoginFailed()
                    }
                }
            }

        })
    }

    override fun validate(): Boolean {
        var valid = true
        val login = input_login.text.toString()
        val password = input_password.text.toString()


        if(login.isEmpty()){
            input_login.error = "Login is empty"
            valid = false
        }
        else if (password.isEmpty()){
            input_password.error = "Password is empty"
            valid = false
        }
        else{
            input_login.error = null
            input_password.error = null
        }

        return valid
    }

    override fun userIsFound(login: String, password: String): Boolean {
        var isFind = false

        mapDataUser.values.forEach{
            if(it.login == login && it.password == password){
                isFind = true
            }
        }

        return isFind
    }

    private fun onLoginSuccess(etapNum: Int, login: String, password: String) {

        val newIntentActivity:Intent =
            if(etapNum == 1)
                Intent(this, OrganizerStartActivity::class.java).apply {
                    putExtra(LOGIN, login)
                    putExtra(PASSWORD, password)
                }
            else
                Intent(this, OrganizerEtapActivity::class.java).apply {
                    putExtra(ETAP, etapNum)
                    putExtra(LOGIN, login)
                    putExtra(PASSWORD, password)
                }

        progress_bar_login.visibility = View.INVISIBLE

        startActivity(newIntentActivity)

        Handler().postDelayed({
            finish()
        }, 1000)
        btn_sign_in.isEnabled = true
    }

    private fun onLoginFailed() {
        Toast.makeText(baseContext, "Login failed", Toast.LENGTH_LONG).show()
        btn_sign_in.isEnabled = true
        progress_bar_login.visibility = View.INVISIBLE
    }
}
