package com.example.relayrace.OrganazerActivity.Start

data class DataStartRequest(var res: ArrayList<StartResources>, var isAvailable: Int?)
data class StartResources(var command_group_name: String?, var etap_id: Int?)