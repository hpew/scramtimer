package com.example.relayrace.OrganazerActivity.Start

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.ListView
import android.widget.TextView
import android.widget.Toast
import com.example.relayrace.OrganazerActivity.DURATION_UPDATE_SEK
import com.example.relayrace.OrganazerActivity.Etap.action
import com.example.relayrace.OrganazerActivity.LOGIN
import com.example.relayrace.OrganazerActivity.PASSWORD
import com.example.relayrace.R
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_organizer_start.*
import okhttp3.*
import java.io.IOException

const val COMMAND_ID = "command_id"

class OrganizerStartActivity : AppCompatActivity() {
    private val TAG_START = "START"
    private val startUrl = "https://relayrace.apisrv.ru/api/mobile/start.php"

    private var mHandler: Handler? = Handler()
    private lateinit var listViewRaces: ListView
    private lateinit var lvoAdapter: ListViewOrganizeAdapter

    private var login = ""
    private var password = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_organizer_start)
        progress_bar_start_organizer.visibility = View.VISIBLE

        //TODO refactor
        getHandler()
        login = intent.getStringExtra(LOGIN)!!
        password = intent.getStringExtra(PASSWORD)!!
        listViewRaces = findViewById(R.id.lv_races)
        lvoAdapter = ListViewOrganizeAdapter(this)
        listViewRaces.adapter = lvoAdapter
    }

    override fun onResume() {
        getHandler()

        permanentRequestStartInfo.run()

        listViewRaces.setOnItemClickListener { adapterView, _, i, _ ->
            progress_bar_start_organizer.visibility = View.VISIBLE
            listViewRaces.visibility = View.GONE
            val etapId = (adapterView.getItemAtPosition(i) as StartResources).etap_id!!
            requestStartInfo(etapId)
        }
        super.onResume()
    }

    override fun onStop() {
        getHandler().removeCallbacks(permanentRequestStartInfo)
        super.onStop()
    }

    override fun onPause() {
        getHandler().removeCallbacks(permanentRequestStartInfo)
        super.onPause()
    }

    override fun onDestroy() {
        getHandler().removeCallbacks(permanentRequestStartInfo)
        super.onDestroy()
    }

    private val permanentRequestStartInfo: Runnable = object : Runnable{
        override fun run() {
            requestStartInfo()
            getHandler().postDelayed(this, 1000 * DURATION_UPDATE_SEK)
        }
    }

    fun createHeader(text: String?): View? {
        val v: View = layoutInflater.inflate(R.layout.lv_header, null)
        (v.findViewById<View>(R.id.tv_header_info) as TextView).text = text
        return v
    }

    private fun getHandler(): Handler{
        if(mHandler == null){
            mHandler = Handler()
        }
        return mHandler as Handler
    }

    private fun requestStartInfo(commandId: Int = 0){
        val client = OkHttpClient()
        val request = Request.Builder()
            .get()
            .url("$startUrl?$LOGIN=$login&$PASSWORD=$password&$COMMAND_ID=$commandId")
            .build()

        client.newCall(request).enqueue(object : Callback{
            override fun onFailure(call: Call, e: IOException) {
                Log.d(TAG_START, e.message.toString())
                runOnUiThread {
                    Toast.makeText(baseContext, "Get data failed", Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call, response: Response) {
                try {
                    val data = Gson().fromJson<DataStartRequest>(response.body?.string(), DataStartRequest::class.java)

                    runOnUiThread {
                        if(data != null) {
                            lvoAdapter.dataUpdate(data.res)
                            progress_bar_start_organizer.visibility = View.INVISIBLE
                            listViewRaces.action(data.isAvailable == 1)
                            if (data.isAvailable == 0) {
                                tv_is_available.text = getString(R.string.races_not_found)
                                tv_is_available.visibility = View.VISIBLE
                            } else {
                                tv_is_available.text = ""
                                tv_is_available.visibility = View.GONE
                            }
                        }
                    }
                }catch (e: Exception){
                    e.printStackTrace()
                    runOnUiThread {
                        Toast.makeText(baseContext, "Fail data parse", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        })
    }
}