package com.example.relayrace

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.relayrace.OrganazerActivity.OrganizerLoginActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observer.setOnClickListener {
            startActivity(Intent(this, ObserverActivity::class.java))
        }
        organizer.setOnClickListener {
            startActivity(Intent(this, OrganizerLoginActivity::class.java))
        }

    }
}